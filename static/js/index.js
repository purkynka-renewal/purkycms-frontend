require('../css/style.forefront.scss');
require('../css/libs-fontawesome.scss');
require('../css/libs-semantic.scss');
require('../css/overrides.scss');
require('../css/style.scss');

// Asynchronously add main CSS
const mainStyle = document.createElement("link");
mainStyle.rel = "stylesheet";
mainStyle.href = document.getElementById("mainStyle").href;
const mainStylePromise = new Promise((resolve)=>mainStyle.onload = resolve);
document.head.appendChild(mainStyle);

// Settings
global.ioOptions = {
  path: "/",
  transports: ['websocket', 'polling'],
};

global.jQuery = global.$ = require('jquery');
const jQueryBridget = (a,b,c)=>import('jquery-bridget').then(d=>a?d.default(a,b,c):a);
require('./libs/semantic.min.js');
const PJAX = require('./PJAX.js');
const locale = require('./locale.js');


//
// SemanticUI settings
//
$.fn.toast.settings.icons = {
  info    : 'fas fa-info',
  success : 'fas fa-checkmark',
  warning : 'fas fa-warning',
  error   : 'fas fa-times'
};
$.fn.toast.settings.showIcon = true;
$.fn.toast.settings.showProgress = "bottom";
$.fn.toast.settings.displayTime = "auto";

//
// Internals
//
// const isMobileMQ = window.matchMedia("(max-width: 720px)");
let background;


//
// Global GUI utility functions
//
global.GUI = {
  error(title, message){
    $("body").toast({class: 'error', message, title });
  },
  success(title, message){
    $("body").toast({class: 'success', message, title });
  }
}

//
// Event handlers
//
// Page content changed
$(document).on("ready.purky", async ()=>{
  mainStylePromise.then(() => $("body").addClass("ready"));

  // Set global config
  updatePathConfig();
  if (background) background.stop();

  // Register service worker if possible
  // if(global.basepath && 'serviceWorker' in navigator) {
  //   navigator.serviceWorker.register(global.basepath+'serviceWorker.js');
  //
  //   // navigator.serviceWorker.ready.then(()=>{
  //   //   $("#offlineInfo").show();
  //   // });
  // }

  // Initiate mobile navbar menu if needed
  if ($("#mainnav-button:not([data-ready])").length) {
    $("#mainnav-button").attr("data-ready", true);

    $("#mainnav-button:visible").on("click", () => {
      $("#mainnav-inner").toggleClass("show");
    });

    $(document).on("scroll", () => {
      $(".mainnav-logo").toggleClass("hide",
          $(document).scrollTop() < $("header#header").height());
    });
  }

  // Locale changer
  locale("#localeSelect", PJAX);

  // Flickity
  if ($("#main-carousel").length) {
    jQueryBridget(); // start download
    import('../css/libs-flickity.scss');
    import('flickity').then(async (a) => {
      const Flickity = a.default;
      Flickity.setJQuery($);
      await jQueryBridget("flickity", Flickity, $);

      const $carousel = $("#main-carousel");
      $carousel.flickity({
        ...$carousel.data("flickity0"),
        resize: false,
      }).on("ready.flickity", () => {
        $carousel.removeClass("hide")
      });
      $("#main-carousel-loader").removeClass("active");
    });
  }

  // Trigger tweaks
  contentUpdate();
})
// Table content changed
.on("tableUpdate.purky", contentUpdate);


//
// Functions
//
function contentUpdate(){
  // Images loaded
  $("main#main img").each(function() {
    const onLoad = () => setTimeout(() => $(this).addClass("loaded"));
    if (this.complete) onLoad();
    else this.onload = onLoad;
  });

  // Transfrom SQL dates to local ones
  $(".date").each(function(){
    const date = new Date($(this).text().trim());
    if (!isNaN(date.valueOf())) $(this).text(date.toLocaleString());
  });

  // Initiate semantic dropdowns (ignores dynamic and already dropdowns)
  $(".ui.dropdown:not([data-list]):not([dropdown])").each(function(){
    $(this).attr("data-dropdown", true).dropdown({
      allowAdditions: $(this).is(".addable"),
      onChange: (...args) => $(this).trigger("change", ...args),
    });
  });

  // Tooltips
  $(".ui.tooltip").popup();

  // Copy To Clipboard
  $("[data-c2c]").off("click").on("click", function() {
    navigator.clipboard.writeText($(this).data("c2c")).then(()=>{
      if($(this).is("[data-result-title]")){
        $(this).popup({
          on: "manual", variation: "green", html: $(this).data("result-title"),
          onHide(){ $(this).popup("remove"); }
        }).popup("show");
      }
    });
  });
}
global.contentUpdate = contentUpdate;

//
// Functions
//
async function initDatatable($table){
  await import('datatables.net-se');
  $.fn.DataTable.ext.errMode = 'none';

  const table = $table.DataTable({
    autoWidth: false,
    pageLength: 14,
    drawCallback: function() {
      const table = this;
      // Select all rows checkbox
      $("th:nth-child(1) input[type='checkbox']", this).off("change")
          .on("change", function() {
        $("tr td:nth-child(1) input[type='checkbox']", table)
            .prop("checked", $(this).prop("checked")).change();
      });
    }
  });

  if ($table.data("searchinput")) {
    $($table.data("searchinput")).on("input", function(){
      table.search($(this).val()).draw();
    });
  }

  if($table.is("[id]")){
    global.dataTableReferences[$table.attr("id")] = table;
  }
}
global.initDatatable = initDatatable;

function updatePathConfig(){
  const $main = $("main#main");
  __webpack_public_path__ = "/static/";//$main.data("assetpath");
  global.ioOptions.path = $main.data("iopath");
  global.filepath = $main.data("filepath");
  global.assetpath = $main.data("assetpath");
  global.basepath = $main.data("basepath");
}
