const Cookies = require("js-cookie");

module.exports = function(selector, PJAX){
  $(selector).not("[data-locale-handled]")
    .attr("data-locale-handled", true)
    .on("change", (_, value) => {
      Cookies.set("locale", value);
      PJAX.reload();
    });
}
