// TODO: PJAX over websockets o_o
let loadLock = false;

// Catch links and load using PJAX if possible
$(document).on("click", "a[href]", {}, function(event){
  const result = loadURL($(this).attr("href"), null, null, this);
  if(result) event.preventDefault();
  return !result;
// Catch forms and process using PJAX if possible
}).on("submit", "form[action][method]", {}, function(event){
  const result = loadURL($(this).attr("action"), $(this).attr("method"),
      $(this).serialize(), this);
  if(result) event.preventDefault();
  return !result;
// Initialize scripts once ready
}).ready(()=>{
  updateActiveLink(window.location.pathname);
  $(document).trigger("ready.purky");
  $("body,main#main").addClass("shown");
});

// React to history browsing
$(window).on("popstate", (event)=>{
  loadURL(event.target.location.pathname, null, null,
      $("<div data-pushState='false'></div>"));
});

/**
 * loadURL - description
 *
 * @param  {type} aUrl description
 * @return {type}      description
 */
function loadURL(aUrl, method="GET", data={}, source){
  if(loadLock) return false;
  loadLock = true;

  if(aUrl.match(/^#/)) {
    loadLock = false;
    return false;
  }

  const url = new URL(absolutizeURL(aUrl));
  const currentUrl = new URL(location.href);
  source = $(source || "<div></div>");
  const isTable = source.is("[data-table]");
  const pushState =source.is("[data-pushState]") ? source.data("pushState") : 1;
  const options = {
    animationStart: source.data("pjax-anim0") || "default",
    animationEnd: source.data("pjax-anim1") || "default",
  };

  // Ignore if not same host
  if(url.host != currentUrl.host){
    // window.open(url.href, "_blank");
    return false;
  }

  $(document).trigger("loadstart.purky");
  $.ajax({url: url.href, method, data,
    beforeSend(jqXHR) {
      // We want just a table
      if (isTable) {
        jqXHR.setRequestHeader("X-Requested-Table", true);
      }
    }
  })
  .done((body, _, jqXHR)=>renderContent(body, jqXHR))
  .fail((jqXHR)=>renderContent(jqXHR.responseText, jqXHR));

  function updateUrl(jqXHR, title){
    if(!pushState) return;
    window.history.pushState({}, title,
      ((jqXHR.getResponseHeader("X-Response-URL") || url.href)+"/")
          .replace(/\/+$/, "/")
    );
  }

  function renderContent(body="", jqXHR){
    $(document).trigger("loadend.purky");

    body = body.trim();
    const $body = $(body);
    const gotWholepage = !$body.is("main#main:first-child");
    const title = $body.data("title") || $("main#main", $body).data("title");

    // The target is a table, not the whole page
    if(!gotWholepage && isTable){
      // Unlock loading next page
      loadLock = false;
      // Render the table
      return rendertable($body, jqXHR);
    }

    // Prepare to animate changed part of page
    const get$target = ()=>gotWholepage ? $("body") : $("main#main");

    // Run animation
    let $target = get$target();
    $target.addClass(options.animationStart).removeClass("shown")
        .one('transitionend', ()=>{
      // Replace the content
      $target.get(0).outerHTML = body;
      $target = get$target().addClass(options.animationEnd);

      // Change title if defined
      if(title) document.title = title;

      // Push new history record // if we were successfull
      // if(String(jqXHR.status).startsWith("2")){
        updateUrl(jqXHR, title);
      // }

      // Finish the animation
      setTimeout(()=>{
        $target.addClass("shown");
        $("main#main", $target).addClass("shown");

        // Wait for the animation to end and clean up
        $target.one('transitionend', ()=>{
          $target.removeClass(options.animationEnd);
          $(document).trigger("transitioned.purky");
        });
      }, 150);

      // Unlock loading next page
      loadLock = false;

      updateActiveLink(aUrl);

      // Tell all scripts that the page has been reloaded
      $(document).trigger("ready.purky");
    });
  }

  function rendertable($body, jqXHR){
    const table = global.dataTableReferences[source.data("table")];
    table.clear();
    table.rows.add($body.children("tr").toArray());
    table.draw();
    $(document).trigger("tableUpdate.purky", $body);
    updateUrl(jqXHR);
  }

  return true;
}


function reload() {
  const url = new URL(window.location);
  url.searchParams.set("fullReload", true);
  return loadURL(url.pathname+url.search, null, null, $("<a/>", {
    "data-pushstate": false
  }));
}


function updateActiveLink(url){
  // Remove prev active class
  $("a[data-pjax-active]")
    .removeClass("active")
    .removeAttr("data-pjax-active");
  // Add active class to active links
  $("a[href='"+url+"'],a[href='"+url+"/'],a[href='"+url.replace(/\/$/, "")+"']")
    .addClass("active")
    .attr("data-pjax-active", true);
}


const urlRegExp = new RegExp("(?<protocol>\\w*:)?" +
    "(?:(?:\\/\\/)?(?<host>\\w[\\w\\-\\.:]+\\.\\w+))?" +
    "(?<uri>\\.?\\.?\\/.*)?");

/**
 * absolutizeURL - Adds domain to uri
 *
 * @param  {String} url description
 * @return {String}     description
 */
function absolutizeURL(url){
  const match = url.match(urlRegExp);
  if(!match) return false;

  const protocol = match.groups.protocol || window.location.protocol;
  const host = match.groups.host || window.location.host;
  const currentPath = window.location.pathname;
  const uri = (match.groups.uri || "")
      .replace(/^\.\./, currentPath.split("/").slice(0,-2).join("/"))
      .replace(/^\./, currentPath)
      .replace(/^\//, "")
      .replace(/\/+/g, /*/ */ "/");

  // don't recreate non-http urls
  if(!protocol.match(/^https?:$/)) return url;
  return protocol+"//"+host+"/"+uri;
}
window.absolutizeURL = absolutizeURL;

module.exports = {loadURL, absolutizeURL, reload};
