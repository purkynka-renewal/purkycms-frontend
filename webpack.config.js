const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { InjectManifest } = require('workbox-webpack-plugin');
const CONFIG = require('./config.json');

module.exports = {
  context: __dirname+"/"+CONFIG.staticPath,
  // mode: "production",
  mode: "development",
  entry: "./js/index.js",
  module: {
    rules: [
      {
        test: /\.s?(a|c)ss$/,
        exclude: /forefront\.scss$/,
        // include: __dirname+"/static/css/index.scss",
        use: [
          MiniCssExtractPlugin.loader,
          'cache-loader',
          'css-loader',
          'sass-loader',
        ],
      },
      {
        include: /forefront\.scss$/,
        use: [
          {
            loader: 'file-loader',
            options: { name: 'css/forefront.boundle.min.css' }
          },
          'cache-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
        use: ['url-loader']
      },
    ],
  },
  output: {
    path: __dirname+"/"+CONFIG.staticPath,
    filename: "js/boundle/[name].boundle.min.js",
  },
  resolve: {
    symlinks: false
  },
  stats: {
    warnings: false
  },
  cache: {
    type: "filesystem",
    buildDependencies: {
      config: [__filename]
    }
  },
  plugins: [
    new CleanWebpackPlugin({cleanOnceBeforeBuildPatterns: ["js/boundle/*"]}),
    new MiniCssExtractPlugin({filename: "css/boundle.min.css"}),
    // Copy SemanticUI assets to static folder
    new CopyWebpackPlugin({patterns: [
      {
        from: '../node_modules/fomantic-ui-sass/src/themes/default/assets/images/',
        to: './img/'
      },
      {
        from: '../node_modules/@fortawesome/fontawesome-free/webfonts/',
        to: './fonts/'
      }
    ]}),
    // new InjectManifest({
    //   swSrc: "./js/serviceWorker.js",
    //   exclude: [/fonts\/fa\-/],
    //   injectionPoint: "__WEBPACK_FILES__",
    //   manifestTransforms: [manifest => ({manifest: manifest.map(item => ({
    //     ...item,
    //     url: "/"+CONFIG.staticPath+item.url,
    //   }))})],
    //   swDest: CONFIG.serviceWorker.path,
    // }),
  ]
};
