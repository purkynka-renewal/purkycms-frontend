// NOTE: DO NOT serve this file from /static/js, always serve from root
// Original source code: https://gist.github.com/JMPerez/8ca8d5ffcc0cc45a8b4e1c279efd8a94

const CACHE_VERSION = 1;
const CURRENT_CACHE = `main-${CACHE_VERSION}`;
const BASE_URL = self.location.pathname.split("/").slice(0,-1).join("/");
const WEBPACK_FILES = __WEBPACK_FILES__;
const {appShell, serviceWorker} = require("../../config.json");
appShell.staticFiles.push(...WEBPACK_FILES.map(item=>item.url));
appShell.staticFiles = appShell.staticFiles.map(item=>BASE_URL+item);
appShell.offlinePage = BASE_URL+appShell.offlinePage;

// on activation we clean up the previously registered service workers
self.addEventListener('activate', event => event.waitUntil(
  caches.keys().then(cacheNames => {
    return Promise.all(
      cacheNames.map(cacheName => {
        if (cacheName != CURRENT_CACHE) {
          return caches.delete(cacheName);
        }
      })
    );
  })
));

// on install we download the routes we want to cache for offline
self.addEventListener('install', event => event.waitUntil(
  caches.open(CURRENT_CACHE).then(cache =>
    cache.addAll(appShell.staticFiles)
  )
));

// fetch the resource from the network
const fromNetwork = (request, timeout) => new Promise((resolve, reject) => {
  const timeoutId = setTimeout(reject, timeout);
  fetch(request).then(response => {
    clearTimeout(timeoutId);
    resolve(response);
    update(request);
  }, reject);
});

// fetch the resource from the browser cache
const fromCache = request => caches.open(CURRENT_CACHE).then(cache =>
  cache
    .match(request)
    .then(matching => matching || cache.match(appShell.offlinePage))
);

// cache the current page to make it available for offline
const update = request => caches.open(CURRENT_CACHE).then(cache =>
  fetch(request).then(response => cache.put(request, response)).catch(()=>{})
);

// general strategy when making a request (eg if online try to fetch it
// from the network with a timeout, if something fails serve from cache)
self.addEventListener('fetch', event => {
  const path = (new URL(event.request.url)).pathname;

  // Serve static files from cache
  if (appShell.staticFiles.some(item=>path.matches(item+"$"))) {
    event.respondWith(fromCache(event.request));
  }
  // Try to download other files
  else {
    event.respondWith(
      fromNetwork(event.request, serviceWorker.requestTimeout)
        .catch(() => fromCache(event.request))
    );
  }

  event.waitUntil(update(event.request));
});
